# POSIX-compatible version of the pyenv Bash shell wrapper
pyenv() {
    case $1 in
        rehash)
            shift
            eval "$(pyenv sh-rehash "$@")"
            ;;
        sh-rehash)
            command pyenv "$@"
            ;;
        shell)
            shift
            eval "$(pyenv sh-shell "$@")"
            ;;
        *)
            command pyenv "$@"
            pyenv rehash
            ;;
    esac
}
