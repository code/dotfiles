# POSIX-compatible version of the plenv Bash shell wrapper
plenv() {
    case $1 in
        rehash)
            shift
            eval "$(plenv sh-rehash "$@")"
            ;;
        sh-rehash)
            command plenv "$@"
            ;;
        shell)
            shift
            eval "$(plenv sh-shell "$@")"
            ;;
        *)
            command plenv "$@"
            plenv rehash
            ;;
    esac
}
