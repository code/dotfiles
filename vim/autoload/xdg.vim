" <https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html#variables>
let s:defaults = {
      \ 'XDG_CACHE_HOME': $HOME.'/.cache',
      \ 'XDG_CONFIG_HOME': $HOME.'/.config',
      \ 'XDG_CONFIG_DIRS': '/etc/xdg',
      \ 'XDG_DATA_HOME': $HOME.'/.local/share',
      \ 'XDG_DATA_DIRS': '/usr/local/share:/usr/share',
      \ 'XDG_STATE_HOME': $HOME.'/.local/state',
      \}

function! s:Get(name) abort
  let name = a:name
  if !has_key(s:defaults, name)
    throw 'Illegal XDG basedirs env var name'
  endif
  let value = s:defaults[name]
  if exists('$'.a:name)
    execute 'let value = $'.a:name
  endif
  return value
endfunction

function! s:Absolute(path) abort
  return a:path =~# '^/'
        \ || a:path =~# '^\~/'
        \ || a:path ==# '~'
endfunction

function! s:Home(name) abort
  let home = s:Get(a:name)
  if !s:Absolute(home)
    return ''
  endif
  return home.'/vim'
endfunction

function! s:Dirs(name) abort
  let dirs = split(s:Get(a:name), ':')
  return map(
        \ filter(copy(dirs), 's:Absolute(v:val)')
        \,'v:val.''/vim'''
        \)
endfunction

function! xdg#CacheHome() abort
  return has('unix') ? s:Home('XDG_CACHE_HOME') : ''
endfunction

function! xdg#ConfigHome() abort
  return has('unix') ? s:Home('XDG_CONFIG_HOME') : ''
endfunction

function! xdg#DataHome() abort
  return has('unix') ? s:Home('XDG_DATA_HOME') : ''
endfunction

function! xdg#StateHome() abort
  return has('unix') ? s:Home('XDG_STATE_HOME') : ''
endfunction

function! xdg#ConfigDirs() abort
  return has('unix') ? s:Dirs('XDG_CONFIG_DIRS') : []
endfunction

function! xdg#DataDirs() abort
  return has('unix') ? s:Dirs('XDG_DATA_DIRS') : []
endfunction
