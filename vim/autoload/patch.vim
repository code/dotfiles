" Wrapper to emulate the nicer has() syntax for simultaneous version and patch
" level checking that was introduced in v7.4.236 and fixed in v7.4.237.
"
" * <https://github.com/vim/vim/releases/tag/v7.4.236>
" * <https://github.com/vim/vim/releases/tag/v7.4.237>
"
function! patch#(version) abort

  " If the Vim running is new enough for its has() function to support
  " checking patch levels with version prefixes, we can just add a "patch-"
  " prefix to the query, and pass it on to has().
  "
  if has('patch-7.4.237')
    return has('patch-'.a:version)
  endif

  " Failing that, we need to do our own version number and patch number
  " comparisons; split the queried version on dots.
  "
  let [major, minor, patch] = split(a:version, '\.')

  " The internal variable v:version describing the running Vim looks like
  " e.g. 801 for v8.1; reproduce that logic for the queried version.
  "
  let l:version = major * 100 + minor

  " If the running version number is the same as the required one, return
  " whether we have the specific patch requested; otherwise, return whether
  " the running version number is greater than the required one.
  "
  return v:version == l:version
        \ ? has('patch-'.patch)
        \ : v:version > l:version

endfunction
