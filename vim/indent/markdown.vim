" Only do this when not yet done for this buffer
if exists('b:did_indent')
  finish
endif
let b:did_indent = 1

" Use four spaces for indentation
call indent#Spaces(4)
