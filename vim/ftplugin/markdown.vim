" Only do this when not yet done for this buffer
if exists('b:did_ftplugin')
  finish
endif

" Don't load the recommended indent styles---not because they're wrong, but
" because they're loaded in the wrong place!  They should be in an indent
" file.  We'll handle that ourselves.
"
let g:markdown_recommended_style = 0

" Do load Markdown folding, even though it's a bit slow
let g:markdown_folding = 1
