" Only do this when not yet done for this buffer--but don't set the flag
" ourselves, either; this is intended to be a prelude to
" $VIMRUNTIME/ftplugin/mail.vim
"
if exists('b:did_ftplugin')
  finish
endif

" Force no_mail_maps value on to work around loading bad maps in
" $VIMRUNTIME/ftplugin/mail.vim
"
if exists('no_mail_maps')
  let b:no_mail_maps = no_mail_maps
endif
let no_mail_maps = 1
