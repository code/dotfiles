" Get matchit.vim, one way or another
if has('packages')
  packadd matchit
else
  runtime macros/matchit.vim
endif
