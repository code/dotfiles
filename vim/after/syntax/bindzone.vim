" Highlight some newer/weirder records correctly
" <https://github.com/vim/vim/issues/220>
syn keyword zoneRRType
      \ contained CAA SSHFP TLSA
      \ nextgroup=zoneRData skipwhite
