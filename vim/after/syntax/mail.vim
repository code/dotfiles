" Don't spellcheck code in mail messages
syntax region mailCode start='`' end='`' keepend contains=@NoSpell
syntax region mailCodeBlock start=' \{4\}' end='$' contains=@NoSpell
