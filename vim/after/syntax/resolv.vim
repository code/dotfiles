" Over-simple but good-enough `nameserver` rule fix including IPv6
" Version 1.4 of the syntax file has a more accurate fix
if !has('patch-8.2.0380')
  syntax clear resolvIPNameserver
  syntax match resolvIPNameserver contained /[0-9.a-fA-F:]\+/
endif
