" The Python runtime files didn't define b:undo_ftplugin until Vim v8.1.1048;
" if it's not set yet, set it here (to something innoccuous) so that the
" appending :let commands in the rest of this file don't break.
"
if !exists('b:undo_ftplugin')
  let b:undo_ftplugin = 'setlocal tabstop<'
endif

" Use pyflakes for syntax checking and autopep8 for tidying
compiler pyflakes
if executable('autopep8')
  setlocal equalprg=autopep8\ -aaa\ --\ -
  let b:undo_ftplugin .= '|setlocal equalprg<'
endif

" Stop here if the user doesn't want ftplugin mappings
if exists('no_plugin_maps') || exists('no_python_maps')
  finish
endif

" Mappings to choose compiler
nnoremap <buffer> <LocalLeader>c
      \ :<C-U>compiler pyflakes<CR>
nnoremap <buffer> <LocalLeader>l
      \ :<C-U>compiler pylint<CR>
let b:undo_ftplugin .= '|nunmap <buffer> <LocalLeader>c'
      \ . '|nunmap <buffer> <LocalLeader>l'

" Mappings to choose 'equalprg'
nnoremap <buffer> <LocalLeader>t
      \ :<C-U>setlocal equalprg=autopep8\ -aaa\ --\ -<CR>
nnoremap <buffer> <LocalLeader>i
      \ :<C-U>setlocal equalprg<<CR>
let b:undo_ftplugin .= '|nunmap <buffer> <LocalLeader>t'
      \ . '|nunmap <buffer> <LocalLeader>i'
