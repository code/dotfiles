" If undofileskip.vim loaded, add a few applicable paths to its list
if !exists('g:undofileskip')
  finish
endif

" Just split and copy 'backupskip'
let g:undofileskip = option#Split(&backupskip)
