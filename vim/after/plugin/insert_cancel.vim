" Set mapping for insert_cancel.vim
if exists('loaded_insert_cancel')
  imap <C-C> <Plug>(InsertCancel)
endif
