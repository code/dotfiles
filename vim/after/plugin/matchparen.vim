" If matchparen.vim didn't load, use 'showmatch' instead
if !exists('loaded_matchparen')
  set showmatch matchtime=3
endif
