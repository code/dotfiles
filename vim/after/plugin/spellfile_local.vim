" Don't make these settings if the base plugin didn't load
if !exists('g:loaded_spellfile_local')
  finish
endif

" Use XDG dirs for 'spellfile' if XDG_DATA_HOME is useable
if xdg#DataHome() !=# ''
  let g:spellfile_local_dirs = [ xdg#DataHome() ]
  call extend(
        \ g:spellfile_local_dirs,
        \ xdg#DataDirs(),
        \)
endif
