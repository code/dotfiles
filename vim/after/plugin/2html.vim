" Don't make these settings if the base plugin didn't load
if !exists('g:loaded_2html_plugin')
  finish
endif

" Set preferred fonts for the HTML rendering
let g:html_font = [
      \ 'DejaVu Sans Mono',
      \ 'Ubuntu Mono',
      \ 'Consolas',
      \]
