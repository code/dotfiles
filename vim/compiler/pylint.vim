" :compiler support for Python linting with pylint
" <https://pypi.org/project/pyflakes/>
if exists('current_compiler') || &compatible || !has('patch-7.4.191')
  finish
endif
let current_compiler = 'pylint'

CompilerSet makeprg=pylint\ --output-format=parseable\ --score=n\ %:S
CompilerSet errorformat=%f:%l:\ %m
