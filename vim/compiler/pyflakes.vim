" :compiler support for Python syntax checking with pyflakes
" <https://pypi.org/project/pyflakes/>
if exists('current_compiler') || &compatible || !has('patch-7.4.191')
  finish
endif
let current_compiler = 'pyflakes'

CompilerSet makeprg=pyflakes\ %:S
CompilerSet errorformat=%f:%l:%c:\ %m
