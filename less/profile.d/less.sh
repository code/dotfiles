# Prompt less(1) to actually look for its config file in the paths the manual
# says it will, as v590 on Debian v12.7 doesn't.
#
LESSKEYIN=${XDG_CONFIG_HOME:-$HOME/.config}/lesskey
export LESSKEYIN
