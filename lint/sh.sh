set \
    sh/profile \
    sh/shinit \
    sh/shrc \
    */profile.d/*.sh \
    */shrc.d/*.sh
shellcheck -e SC1090 -e SC1091 -s sh -- "$@" || exit
