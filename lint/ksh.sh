set \
    ksh/kshrc \
    ksh/kshrc.d/*.ksh
shellcheck -e SC1090 -e SC1091 -s ksh -- "$@" || exit
shellcheck -e SC1090 -e SC1091 -s sh -- ksh/shrc.d/ksh.sh || exit
