# POSIX shell
set --
for game in games/*.sh ; do
    set -- "$@" "${game%.sh}"
done
shellcheck -e SC1090 -e SC1091 -- "$@" || exit
