# Set home directory for cpanm files to be XDG-conformant
PERL_CPANM_HOME=${XDG_CACHE_HOME:-$HOME/.cache}/cpanm
export PERL_CPANM_HOME
