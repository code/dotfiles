#
# Custom i3wm X session to fit into Debian's way of doing things, for use with
# startx(1) or xinit(1).  From Debian's `man 1 startx` (xinit dpkg 1.4.0-1):
#
# > Note that in the Debian system, what many people traditionally put in the
# > .xinitrc file should go in .xsession instead; this permits the same X
# > environment to be presented whether startx, xdm, or xinit is used to start
# > the X session.  All discussion of the .xinitrc file in the xinit(1) manual
# > page applies equally well to .xsession.
#
# This turns out to be important for having the X session wrappers in /etc/X11
# on Debian-derived systems do things like dbus daemon and accessibility setup.
#
# At the time of writing, none of my machines running X are using anything
# other than the Debian-derived X startup script layout, so we'll just conform
# to that unless and until I actually need to abstract this.
#

# Monitor and wallpaper setup is very machine-specific, and isn't versioned in
# here.  Neither xrandr(1) nor xwallpaper(1) have config files, so we fake it
# with xargs and looking for a file with argument tokens to read in
# XDG_CONFIG_HOME, which will almost always be:
#
# - ~/.config/xrandr/config
# - ~/.config/xwallpaper/config
#
xargs xrandr \
    < "${XDG_CONFIG_HOME:-"$HOME"/.config}"/xrandr/config
xargs xwallpaper \
    < "${XDG_CONFIG_HOME:-"$HOME"/.config}"/xwallpaper/config

# Set a few X user preferences:
#
# - No bell
# - Power management on, but start with no timeouts
# - Quick curved mouse acceleration
# - No screen saver
#
xset \
    b off \
    dpms 0 0 0 \
    mouse 5/2 0 \
    s off

# Start a few daemons if we can; it's OK if any of these don't exist, but we'll
# log the failed attempt to start them to the errors file, as a hint that
# I might want to install them.
#
## Message display (libnotify)
dunst &
## PulseAudio system tray tool
pasystray &
## Hide mouse after inactivity
unclutter &

# Load all supplementary scripts in ~/.xsession.d
for sh in "$HOME"/.xsession.d/*.sh ; do
    [ -e "$sh" ] || continue
    . "$sh"
done
unset -v sh

# Become an i3 window manager process, having set everything else up
exec i3
