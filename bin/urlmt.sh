# Get the MIME type for a given URL
self=urlmt
if [ "$#" -ne 1 ] || [ -z "$1" ] ; then
    printf >&2 '%s: Need a single URL\n' \
        "$self"
    exit 2
fi
url=$1
curl --head --output /dev/null --write-out '%{content_type}\n' "$url" |
    awk -F\; '{print $1}'
