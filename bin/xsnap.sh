# Wrapper for scrot(1) for use in recording screenshots
date=$(date +%Y/%m/%d) || exit
dir=${XDG_DATA_HOME:-"$HOME"/.local/share}/scrot/screenshots/$date
mkdir -p -- "$dir" || exit
cd -- "$dir" || exit
for ent in * ; do
    [ -e "$ent" ] || continue
    case $ent in
        *.png) ;;
        *) continue ;;
    esac
    basename=${ent%.png}
    case $basename in
        *[!0-9]*) continue ;;
    esac
    last=$basename
done
number=$(printf '%s' "$last" | sed 's/^0*//')  # Avoid octal
id=$((number + 1))
name=$(printf '%05d' "$id").png
path=$dir/$name
scrot --file="$path" --overwrite "$@" || exit
xclip -selection clipboard -target image/png "$path" || exit
notify-send "Saved: $date/$name"
