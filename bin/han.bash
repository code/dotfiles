# Abstract calls to Bash help vs man(1)

# Ensure we're using at least version 3.0
# shellcheck disable=SC2128
[ -n "$BASH_VERSINFO" ] || exit    # Check version array exists (>=2.0)
((BASH_VERSINFO[0] >= 3)) || exit  # Check actual major version number

# Figure out the options with which we can call help; Bash >=4.0 has an -m
# option which prints the help output in a man-page like format
declare -a helpopts
if ((BASH_VERSINFO[0] >= 4)) ; then
    helpopts=(-m)
fi

# Call `help`, with the `-m` flag if available; if it errors out (discard
# stderr), run `man` instead
help "${helpopts[@]}" -- "$@" 2>/dev/null || man -- "$@"
