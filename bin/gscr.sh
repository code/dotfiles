# Scrub and pack Git repositories

# Iterate through given directories; default to the current one
for arg in "${@:-.}" ; do (

    # Note the "exit" calls here in lieu of "continue" are deliberate; we're in
    # a subshell, so leaving it will continue the loop.

    # Enter either bare repository or .git subdir
    case $arg in
        *.git)
            cd -- "$arg" || exit
            ;;
        *)
            cd -- "$arg"/.git || exit
            ;;
    esac

    # Remove any original refs from a prior rewrite
    if [ -e refs/original ] ; then
        rm -r -- refs/original || exit
    fi

    # Check for bad references or other integrity/sanity problems
    git fsck --full --no-reflogs || exit

    # Expire dangling references
    git reflog expire --all --expire=now --expire-unreachable=now || exit

    # Remove dangling references
    git gc --prune=now --aggressive || exit

) done
