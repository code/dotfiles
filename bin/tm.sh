# Attach to existing tmux session rather than create a new one if possible

# Source config if it exists (just the session name, really)
if [ -e "${XDG_CONFIG_HOME:-"$HOME"/.config}"/tm/config ] ; then
    . "${XDG_CONFIG_HOME:-"$HOME"/.config}"/tm/config || exit
fi

# If given any arguments, just use them as they are
if [ "$#" -gt 0 ] ; then
    :

# If a session exists, just attach to it
elif tmux has-session 2>/dev/null ; then
    set -- attach-session -d

# Create a new session with an appropriate name
else
    set -- new-session -s "${default_session_name:-default}"
fi

# Execute with concluded arguments
exec tmux "$@"
