# Crudely but quickly count mail in the user's inbox, if we can find it
username=$(id -nu)
if [ "$#" -eq 0 ] ; then
    set -- "$MAIL" /var/mail/"$username" /var/spool/mail/"$username"
fi
for path do
    [ -e "$path" ] || continue
    grep -ch -- '^From ' "$path"
    exit
done
printf >&2 'Couldn'\''t find user mail spool; provide it as an argument...?\n'
exit 1
