restore() {
    if [ -n "$stty" ] ; then
        stty "$stty"
    fi
}
trap restore EXIT
if [ -t 0 ] ; then
    stty=$(stty -g)
    stty -echo
fi
cat
