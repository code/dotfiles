# Print all the hosts from ssh_config(1) files

# Manage the processing flag (starts set in each file)
BEGIN { sls = 1 }
FNR == 1 { sls = 1 }
/### sls/ { sls = 1 }
/### nosls/ { sls = 0 }

# Skip if we're ignoring hosts
!sls { next }
# Skip if this isn't a host line
$1 != "Host" { next }

# Add all the patterns after the keyword that don't have wildcards
{
    for (i = 2; i <= NF; i++) {
        if ($i !~ /[?*]/) {
            hosts[$i]++
        }
    }
}

# Print the complete list of hosts, sorted
END {
    for (host in hosts) {
        print host | "sort"
    }
}
