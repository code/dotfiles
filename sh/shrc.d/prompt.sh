# Some systems' /etc/profile setups export their prompt strings (PS1, PS2...),
# which really fouls things up when switching between non-login shells; let's
# put things right by unsetting each of them to break the export, and then just
# setting them as simple variables
unset PS1
PS1='$ '
unset PS2
PS2='> '
unset PS3
PS3='? '
unset PS4
PS4='+ '

# Prompt has hostname via SSH outside of screen/tmux
if [ -n "$SSH_CLIENT" ] && [ -z "$STY" ] && [ -z "$TMUX" ] ; then
    PS1=$(hostname -s)$PS1
fi
