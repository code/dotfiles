# Manage Torsocks for the current shell
tor() {

    # Check first argument to figure out operation
    case $1 in

        # Show whether Torsocks
        show|'')
            case $LD_PRELOAD: in
                (*/libtorsocks.so:*)
                    printf 'on\n'
                    ;;
                (*)
                    printf 'off\n'
                    ;;
            esac
            ;;

        # Turn Torsocks on or off
        on|off)
            command -v torsocks >/dev/null 2>&1 || return
            . "$(command -v torsocks)"
            ;;

        # Command not found
        *)
            printf >&2 \
                'tor(): %s: Unknown command (try "help")\n' \
                "$1"
            return 2
            ;;
    esac
}
