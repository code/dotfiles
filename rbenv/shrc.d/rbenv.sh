# POSIX-compatible version of the rbenv Bash shell wrapper
rbenv() {
    case $1 in
        rehash)
            shift
            eval "$(rbenv sh-rehash "$@")"
            ;;
        sh-rehash)
            command rbenv "$@"
            ;;
        shell)
            shift
            eval "$(rbenv sh-shell "$@")"
            ;;
        *)
            command rbenv "$@"
            rbenv rehash
            ;;
    esac
}
