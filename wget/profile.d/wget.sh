# Set path to wgetrc file in XDG dirs.  There's no mention of this environment
# variable in the man page!  I had to check the source.
#
WGETRC=${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc
export WGETRC
