# ADVENTURE
dir=${XDG_CONFIG_HOME:-$HOME/.config}/xyzzy
if ! [ -d "$dir" ] ; then
    mkdir -- "$dir" || exit
fi
if [ -e "$dir"/dest ] ; then
    printf >&2 'Nothing happens.\n'
else
    printf 'I see no cave here.\n' > "$dir"/dest
fi
exit 1
