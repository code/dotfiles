set \
    sh/profile \
    sh/shinit \
    sh/shrc \
    */profile.d/*.sh \
    */shrc.d/*.sh
for sh do
    sh -n -- "$sh" || exit
done
