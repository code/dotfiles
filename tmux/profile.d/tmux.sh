# Encourage tmux to put its sockets into XDG_RUNTIME_DIR rather than /tmp
[ -n "$XDG_RUNTIME_DIR" ] || return
TMUX_TMPDIR=$XDG_RUNTIME_DIR
export TMUX_TMPDIR
