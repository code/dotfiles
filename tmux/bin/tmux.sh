#
# Simple wrapper to force acting only as client; can't do anything if the
# server isn't actually running.  The correct way to do this is of course
# socket activation, but Debian's version doesn't have --enable-systemd yet.
# This will do fine in the meantime.
#
if ! systemctl --user is-active --quiet tmux.service ; then
    printf >&2 'tmux: server not running\n'
    exit
fi
exec /usr/bin/tmux "$@"
