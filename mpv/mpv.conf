# Don't try to show me album covers when I play FLAC
no-audio-display

# Use yt-dlp instead of youtube-dl, as the latter project appears to have been
# abandonded for some time as at 2022-05-08.
#
# * <https://github.com/ytdl-org/youtube-dl/issues/29965>
# * <https://web.archive.org/web/20220212025407/https://github.com/ytdl-org/youtube-dl/issues/29965>
#
script-opts-append="ytdl_hook-ytdl_path=yt-dlp"

# Save screenshot images into XDG-respecting location
screenshot-directory="~/.local/share/mpv/screenshots"

# Use PNG (lossless) for screenshots rather than the default JPEG
screenshot-format="png"

# Keep the screenshots in directories named after the media file, with the
# screenshot filename including the media filename, the timestamp within it,
# and a numeric suffix to guarantee uniqueness.  Full paths look like this:
#
# ~/.local/share/mpv/screenshots/Red Dwarf S01E01.mkv/mpv_Red Dwarf S01E01.mkv_00:08:20.461_0001.png
#
screenshot-template="%f/mpv_%f_%P_%04n"

# Don't try to find external subtitles for me; I'll specify if I want them.
sub-auto=no

# Get somewhat lower-resolution (<=640kps), lower-bitrate (<=500kbps) videos,
# so that they come down quicker and slower systems are less likely to stutter
# while playing them at busy times; if I want archival quality, I'll just
# invoke yt-dlp directly.
#
ytdl-format="[height<=?640][tbr<=?500]"
