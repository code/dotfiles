# Complete ssh_config(5) hostnames
_ssh_config_hosts() {

    # Iterate through SSH client config paths
    local config
    for config in /etc/ssh/ssh_config.d/*.conf /etc/ssh/ssh_config \
            "$HOME"/.ssh/config.d/*.conf "$HOME"/.ssh/config ; do
        [[ -e $config ]] || continue

        # Read 'Host' options and their patterns from file
        local option value patterns pattern ci
        while read -r option value ; do
            [[ $option == Host ]] || continue
            read -a patterns -r \
                < <(printf '%s\n' "$value")

            # Check host value
            for pattern in "${patterns[@]}" ; do
                case $pattern in
                    # No empties
                    '') ;;
                    # No wildcards
                    *'*'*) ;;
                    # Found a match; print it
                    "$2"*) COMPREPLY[ci++]=$pattern ;;
                esac
            done

        done < "$config"
    done
}
