# Build groups from address lists
for rc in "$HOME"/.config/mutt/muttrc.d/groups/*.list ; do
    [ -e "$rc" ] || continue
    group=$rc
    group=${group##*/}
    group=${group%.list}
    while read -r member ; do
        printf 'group -group %s -addr %s\n' \
            "$group" "$member"
    done < "$rc"
done

# Emit the source all muttrc subfiles, in LC_COLLATE order
for rc in "$HOME"/.config/mutt/muttrc.d/*.rc ; do
    [ -e "$rc" ] || continue
    cat -- "$rc"
done

# Special case: if using GNU Emacs, emit a line to disable edit_headers, just
# because at the moment I haven't worked out how to make this work sanely with
# messages-are-flowing in message-mode.
case $VISUAL in
    emacs|emacsclient) printf 'unset edit_headers\n' ;;
esac
