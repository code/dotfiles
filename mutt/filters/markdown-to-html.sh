printf 'text/html\n\n'
sed 's/  $/ /;/[^ ]$/s/$/  /;s/^-- $/--  /' |
pandoc \
    --from markdown_strict+smart \
    --metadata=pagetitle:HTML \
    --standalone \
    --to html4 |
sed '/[—–][—–]/{s/—/---/g;s/–/--/g}'
