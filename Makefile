.POSIX:

.PHONY: all \
	clean \
	distclean \
	install \
	install-abook \
	install-bash \
	install-bash-completion \
	install-bin \
	install-bin-man \
	install-cabal \
	install-cargo \
	install-cpanm \
	install-curl \
	install-dillo \
	install-emacs \
	install-ex \
	install-finger \
	install-games \
	install-games-man \
	install-git \
	install-gnupg \
	install-i3 \
	install-init \
	install-ksh \
	install-less \
	install-login-shell \
	install-mail \
	install-man \
	install-mpv \
	install-mutt \
	install-mysql \
	install-ncmpcpp \
	install-newsboat \
	install-parcellite \
	install-perlcritic \
	install-perltidy \
	install-plenv \
	install-psql \
	install-pyenv \
	install-rbenv \
	install-readline \
	install-redshift \
	install-rofi \
	install-scrot \
	install-sh \
	install-subversion \
	install-sxhkd \
	install-systemd \
	install-tidy \
	install-tmux \
	install-vim \
	install-vim-gui \
	install-vint \
	install-wget \
	install-x \
	install-xsession \
	install-zsh \
	check \
	check-bash \
	check-bin \
	check-games \
	check-git-template-hooks \
	check-ksh \
	check-login-shell \
	check-man \
	check-sh \
	check-x \
	check-xsession \
	check-zsh \
	lint \
	lint-bash \
	lint-bin \
	lint-games \
	lint-git-template-hooks \
	lint-ksh \
	lint-sh \
	lint-vim \
	lint-x \
	lint-xsession

.SUFFIXES:
.SUFFIXES: .awk .bash .m4 .mi5 .pl .sed .sh

XDG_CACHE_HOME = $(HOME)/.cache
XDG_CONFIG_HOME = $(HOME)/.config
XDG_DATA_HOME = $(HOME)/.local/share

NAME = 'Tom Ryder'
EMAIL = tom@sanctum.geek.nz
GPG_KEYID = FA09C06E1B670CD0B2F5DE60C14286EA77BB8872

BINS = bin/ap \
	bin/apf \
	bin/ax \
	bin/bcq \
	bin/bel \
	bin/bl \
	bin/bp \
	bin/br \
	bin/brnl \
	bin/ca \
	bin/cf \
	bin/cfr \
	bin/chc \
	bin/chn \
	bin/clog \
	bin/clrd \
	bin/clwr \
	bin/csmw \
	bin/dam \
	bin/d2u \
	bin/ddup \
	bin/defang \
	bin/dfv \
	bin/dub \
	bin/edda \
	bin/eds \
	bin/exm \
	bin/fgscr \
	bin/finc \
	bin/fnl \
	bin/fnp \
	bin/gms \
	bin/grc \
	bin/grec \
	bin/gred \
	bin/gscr \
	bin/gwp \
	bin/hms \
	bin/htdec \
	bin/htenc \
	bin/htref \
	bin/hurl \
	bin/igex \
	bin/isgr \
	bin/jfc \
	bin/jfcd \
	bin/jfp \
	bin/loc \
	bin/mi5 \
	bin/max \
	bin/maybe \
	bin/mean \
	bin/med \
	bin/mex \
	bin/mftl \
	bin/mim \
	bin/min \
	bin/mkcp \
	bin/mked \
	bin/mkmv \
	bin/mktd \
	bin/mkvi \
	bin/mode \
	bin/motd \
	bin/msc \
	bin/murl \
	bin/mw \
	bin/nlbr \
	bin/oii \
	bin/onl \
	bin/osc \
	bin/pa \
	bin/paz \
	bin/ped \
	bin/phpcsff \
	bin/pit \
	bin/p \
	bin/pp \
	bin/pph \
	bin/pst \
	bin/pvi \
	bin/pwg \
	bin/qat \
	bin/quo \
	bin/rep \
	bin/rfcf \
	bin/rfcr \
	bin/rfct \
	bin/rgl \
	bin/rnda \
	bin/rndf \
	bin/rndi \
	bin/rndl \
	bin/rnds \
	bin/sd2u \
	bin/sec \
	bin/shb \
	bin/slow \
	bin/sls \
	bin/slsf \
	bin/sqs \
	bin/sra \
	bin/sshi \
	bin/sta \
	bin/stbl \
	bin/stex \
	bin/stws \
	bin/su2d \
	bin/sue \
	bin/supp \
	bin/swr \
	bin/td \
	bin/tl \
	bin/tlcs \
	bin/tm \
	bin/tot \
	bin/trs \
	bin/try \
	bin/u2d \
	bin/umake \
	bin/unf \
	bin/urlc \
	bin/urlh \
	bin/urlmt \
	bin/uts \
	bin/vest \
	bin/vex \
	bin/vic \
	bin/wro \
	bin/xgo \
	bin/xgoc \
	bin/xrbg \
	bin/xrq \
	bin/xsnap

BINS_M4 = bin/chn.m4 \
	bin/dfv.m4 \
	bin/edda.m4 \
	bin/mim.m4 \
	bin/oii.m4 \
	bin/phpcsff.m4 \
	bin/pst.m4 \
	bin/swr.m4 \
	bin/tlcs.m4 \
	bin/try.m4 \
	bin/urlc.m4

BINS_SH = bin/chn.sh \
	bin/dfv.sh \
	bin/edda.sh \
	bin/mim.sh \
	bin/oii.sh \
	bin/phpcsff.sh \
	bin/pst.sh \
	bin/swr.sh \
	bin/tlcs.sh \
	bin/try.sh \
	bin/urlc.sh

GAMES = games/aaf \
	games/acq \
	games/aesth \
	games/chkl \
	games/dr \
	games/drakon \
	games/kvlt \
	games/philsay \
	games/pks \
	games/rndn \
	games/rot13 \
	games/uuu \
	games/squ \
	games/strik \
	games/xyzzy \
	games/zs

GIT_TEMPLATE_HOOKS = git/template/hooks/post-update \
	git/template/hooks/pre-commit \
	git/template/hooks/prepare-commit-msg

all: $(BINS) \
	$(GIT_TEMPLATE_HOOKS) \
	git/config \
	gnupg/profile.d/gnupg.sh \
	tmux/bin/tmux \
	x/xsession

clean distclean:
	rm -f -- \
		$(BINS) \
		$(BINS_M4) \
		$(BINS_SH) \
		$(GAMES) \
		$(GIT_TEMPLATE_HOOKS) \
		bin/han \
		dillo/dillorc \
		dillo/dillorc.m4 \
		git/config \
		git/config.m4 \
		gnupg/profile.d/gnupg.sh \
		include/mktd.m4 \
		mutt/filters/markdown-to-html \
		mutt/muttrc.d/src \
		tmux/bin/tmux \
		x/xsession

.awk:
	sh bin/shb.sh awk -f < $< > $@
	chmod +x ./$@

.bash:
	sh bin/shb.sh bash < $< > $@
	chmod +x ./$@

.pl:
	sh bin/shb.sh perl < $< > $@
	chmod +x ./$@

.sed:
	sh bin/shb.sh sed -f < $< > $@
	chmod +x ./$@

.sh:
	sh bin/shb.sh sh < $< > $@
	chmod +x ./$@

.mi5.m4:
	awk -f bin/mi5.awk < $< > $@

.m4.sh:
	m4 < $< > $@

bin/chn.sh: bin/chn.m4 include/mktd.m4
bin/dfv.sh: bin/dfv.m4 include/mktd.m4
bin/edda.sh: bin/edda.m4 include/mktd.m4
bin/mim.sh: bin/mim.m4 include/mktd.m4
bin/oii.sh: bin/oii.m4 include/mktd.m4
bin/phpcsff.sh: bin/phpcsff.m4 include/mktd.m4
bin/pst.sh: bin/pst.m4 include/mktd.m4
bin/swr.sh: bin/swr.m4 include/mktd.m4
bin/tlcs.sh: bin/tlcs.m4 include/mktd.m4
bin/try.sh: bin/try.m4 include/mktd.m4
bin/urlc.sh: bin/urlc.m4 include/mktd.m4

dillo/dillorc: dillo/dillorc.m4
	m4 \
		-D HOME=$(HOME) \
		dillo/dillorc.m4 > $@

git/config: git/config.m4
	m4 \
		-D NAME=$(NAME) \
		-D EMAIL=$(EMAIL) \
		-D GPG_KEYID=$(GPG_KEYID) \
		-D XDG_DATA_HOME=$(XDG_DATA_HOME) \
		git/config.m4 > $@

gnupg/profile.d/gnupg.sh: gnupg/profile.d/gnupg.sh.m4
	m4 \
		-D GPG_KEYID=$(GPG_KEYID) \
		gnupg/profile.d/gnupg.sh.m4 > $@

MAILDIR = $(HOME)/Mail

install: install-bin \
	install-curl \
	install-ex \
	install-git \
	install-gnupg \
	install-init \
	install-less \
	install-login-shell \
	install-man \
	install-readline \
	install-vim

install-conf:
	sh install/conf.sh

install-abook:
	mkdir -p -- $(HOME)/.abook
	cp -p -- abook/abookrc $(HOME)/.abook

install-bash: check-bash install-sh bin/han
	mkdir -p -- $(HOME)/.bashrc.d $(HOME)/.local/bin
	cp -p -- bash/bashrc $(HOME)/.bashrc
	cp -p -- bash/bashrc.d/*.bash $(HOME)/.bashrc.d
	cp -p -- bash/bash_profile $(HOME)/.bash_profile
	cp -p -- bash/bash_logout $(HOME)/.bash_logout
	cp -p -- bin/han $(HOME)/.local/bin

install-bash-completion: install-bash
	mkdir -p -- $(HOME)/.bash_completion.d $(XDG_CONFIG_HOME)
	cp -p -- bash/bash_completion $(XDG_CONFIG_HOME)
	cp -p -- bash/bash_completion.d/*.bash $(HOME)/.bash_completion.d

install-bin: $(BINS) install-bin-man
	mkdir -p -- $(HOME)/.local/bin
	cp -p -- $(BINS) $(HOME)/.local/bin

install-bin-man:
	mkdir -p -- $(XDG_DATA_HOME)/man/man1 $(XDG_DATA_HOME)/man/man8
	cp -p -- man/man1/*.1df $(XDG_DATA_HOME)/man/man1
	cp -p -- man/man8/*.8df $(XDG_DATA_HOME)/man/man8

install-cabal:
	cp -p -- cabal/profile.d/cabal.sh $(HOME)/.profile.d

install-cargo:
	cp -p -- cargo/profile.d/cargo.sh $(HOME)/.profile.d

install-cpanm:
	cp -p -- cpanm/profile.d/cpanm.sh $(HOME)/.profile.d

install-curl:
	cp -p -- curl/curlrc $(HOME)/.curlrc

install-dillo: dillo/dillorc
	mkdir -p -- $(HOME)/.dillo
	cp -p -- dillo/dillorc $(HOME)/.dillo/dillorc

install-emacs: emacs/bin/emacsm install-sh
	mkdir -p -- $(HOME)/.local/bin
	cp -p -- emacs/emacs $(HOME)/.emacs
	cp -p -- emacs/bin/emacsm $(HOME)/.local/bin
	cp -p -- emacs/profile.d/emacs.sh $(HOME)/.profile.d

install-ex:
	cp -p -- ex/exrc $(HOME)/.exrc

install-finger:
	cp -p -- finger/plan $(HOME)/.plan
	cp -p -- finger/project $(HOME)/.project
	cp -p -- finger/pgpkey $(HOME)/.pgpkey

install-games: $(GAMES) install-games-man
	mkdir -p -- $(HOME)/.local/games
	cp -p -- $(GAMES) $(HOME)/.local/games

install-games-man:
	mkdir -p -- $(XDG_DATA_HOME)/man/man6
	cp -p -- man/man6/*.6df $(XDG_DATA_HOME)/man/man6

install-git: git/config $(GIT_TEMPLATE_HOOKS)
	mkdir -p -- $(XDG_CONFIG_HOME)/git
	cp -p -- git/config $(XDG_CONFIG_HOME)/git/config
	mkdir -p -- $(XDG_DATA_HOME)/git/template
	cp -pR -- git/template/description \
		$(XDG_DATA_HOME)/git/template/description
	mkdir -p -- $(XDG_DATA_HOME)/git/template/hooks
	cp -pR -- $(GIT_TEMPLATE_HOOKS) \
		$(XDG_DATA_HOME)/git/template/hooks

install-gnupg: gnupg/profile.d/gnupg.sh install-sh
	cp -p -- gnupg/profile.d/gnupg.sh $(HOME)/.profile.d

install-i3: install-x
	mkdir -p -- $(XDG_CONFIG_HOME)/i3
	cp -p -- i3/config $(XDG_CONFIG_HOME)/i3
	mkdir -p -- $(XDG_CONFIG_HOME)/i3status
	cp -p -- i3/status $(XDG_CONFIG_HOME)/i3status/config

install-init:
	if test -d /run/systemd/system ; then make install-systemd ; fi

install-less:
	cp -p -- less/profile.d/less.sh $(HOME)/.profile.d
	cp -p -- less/lesskey $(XDG_CONFIG_HOME)/lesskey

install-logrotate: install-systemd
	mkdir -p -- $(XDG_CONFIG_HOME)/logrotate \
		$(XDG_CONFIG_HOME)/logrotate/config.d
	cp -p -- logrotate/config $(XDG_CONFIG_HOME)/logrotate
	cp -p -- logrotate/systemd/user/logrotate.service \
		logrotate/systemd/user/logrotate.timer \
		$(XDG_DATA_HOME)/systemd/user

install-mail:
	cp -p -- mail/signature $(HOME)/.signature

install-man:
	mkdir -p -- $(XDG_DATA_HOME)/man/man7
	cp -p -- man/man7/dotfiles.7df $(XDG_DATA_HOME)/man/man7

install-mpv:
	mkdir -p -- \
		$(XDG_CONFIG_HOME)/mpv \
		$(XDG_DATA_HOME)/mpv/screenshots
	cp -p -- mpv/mpv.conf $(XDG_CONFIG_HOME)/mpv

install-mutt: install-gnupg install-mail mutt/filters/markdown-to-html mutt/muttrc.d/src
	mkdir -p -- \
		$(XDG_CONFIG_HOME)/mutt \
		$(XDG_CONFIG_HOME)/mutt/muttrc.d \
		$(XDG_CACHE_HOME)/mutt/headers \
		$(XDG_DATA_HOME)/mutt/autocrypt \
		$(HOME)/.local/libexec/mutt/filters
	cp -p -- mutt/muttrc \
		mutt/mailcap \
		$(XDG_CONFIG_HOME)/mutt
	cp -p -- mutt/muttrc.d/src \
		$(XDG_CONFIG_HOME)/mutt/muttrc.d
	touch -- $(XDG_CONFIG_HOME)/mutt/aliases
	cp -p -- mutt/filters/markdown-to-html \
		$(HOME)/.local/libexec/mutt/filters

install-ncmpcpp:
	mkdir -p -- $(XDG_CONFIG_HOME)/ncmpcpp
	cp -p -- ncmpcpp/config $(XDG_CONFIG_HOME)/ncmpcpp

install-newsboat: install-systemd
	mkdir -p -- $(XDG_CONFIG_HOME)/newsboat \
	$(XDG_DATA_HOME)/newsboat
	cp -p -- newsboat/config \
		$(XDG_CONFIG_HOME)/newsboat
	cp -p -- newsboat/systemd/user/newsboat.service \
		$(XDG_DATA_HOME)/systemd/user

install-mysql:
	cp -p -- mysql/my.cnf $(HOME)/.my.cnf

install-ksh: check-ksh install-sh
	mkdir -p -- $(HOME)/.kshrc.d
	cp -p -- ksh/shrc.d/ksh.sh $(HOME)/.shrc.d
	cp -p -- ksh/kshrc $(HOME)/.kshrc
	cp -p -- ksh/kshrc.d/*.ksh $(HOME)/.kshrc.d

install-login-shell: check-login-shell
	sh install/login-shell.sh

install-parcellite: install-x
	cp -p -- parcellite/parcelliterc $(XDG_CONFIG_HOME)
	cp -p -- parcellite/xsession.d/parcellite.sh $(HOME)/.xsession.d

install-perlcritic: install-sh
	cp -p -- perlcritic/profile.d/perlcritic.sh $(HOME)/.profile.d
	mkdir -p -- $(XDG_CONFIG_HOME)/perlcritic
	cp -p -- perlcritic/perlcriticrc $(XDG_CONFIG_HOME)/perlcritic/perlcriticrc

install-perltidy: install-sh
	cp -p -- perltidy/profile.d/perltidy.sh $(HOME)/.profile.d
	mkdir -p -- $(XDG_CONFIG_HOME)/perltidy
	cp -p -- perltidy/perltidyrc $(XDG_CONFIG_HOME)/perltidy/perltidyrc

install-plenv: install-sh install-cpanm
	cp -p -- plenv/profile.d/plenv.sh $(HOME)/.profile.d
	cp -p -- plenv/shrc.d/plenv.sh $(HOME)/.shrc.d

install-psql:
	cp -p -- psql/psqlrc $(HOME)/.psqlrc

install-pyenv: install-sh
	cp -p -- pyenv/profile.d/pyenv.sh $(HOME)/.profile.d
	cp -p -- pyenv/shrc.d/pyenv.sh $(HOME)/.shrc.d

install-rbenv: install-sh
	cp -p -- rbenv/profile.d/rbenv.sh $(HOME)/.profile.d
	cp -p -- rbenv/shrc.d/rbenv.sh $(HOME)/.shrc.d

install-readline:
	cp -p -- readline/inputrc $(HOME)/.inputrc

install-redshift: install-x
	cp -p -- redshift/redshift.conf $(XDG_CONFIG_HOME)
	cp -p -- redshift/xsession.d/redshift.sh $(HOME)/.xsession.d

install-rofi: rofi/bin/rofi_pass
	mkdir -p -- $(HOME)/.local/bin
	cp -p -- rofi/bin/rofi_pass $(HOME)/.local/bin

install-scrot:
	mkdir -p -- $(XDG_DATA_HOME)/scrot/screenshots

install-sh: check-sh
	cp -p -- sh/profile $(HOME)/.profile
	mkdir -p -- $(HOME)/.profile.d
	cp -p -- sh/profile.d/*.sh $(HOME)/.profile.d
	cp -p -- sh/shinit $(HOME)/.shinit
	cp -p -- sh/shrc $(HOME)/.shrc
	mkdir -p -- $(HOME)/.shrc.d
	cp -p -- sh/shrc.d/*.sh $(HOME)/.shrc.d

install-subversion:
	mkdir -p -- $(HOME)/.subversion
	cp -p -- subversion/config $(HOME)/.subversion

install-sxhkd: install-scrot install-x
	mkdir -p -- $(XDG_CONFIG_HOME)/sxhkd
	cp -p -- sxhkd/sxhkdrc $(XDG_CONFIG_HOME)/sxhkd
	cp -p -- sxhkd/xsession.d/sxhkd.sh $(HOME)/.xsession.d

install-systemd: install-sh
	cp -p -- systemd/profile.d/systemd.sh $(HOME)/.profile.d
	mkdir -p -- $(XDG_DATA_HOME)/systemd/user
	cp -p -- systemd/user/notify-email@.service \
		$(XDG_DATA_HOME)/systemd/user
	mkdir -p -- $(XDG_DATA_HOME)/systemd/user/service.d
	cp -p -- systemd/user/service.d/50-notify-email.conf \
		$(XDG_DATA_HOME)/systemd/user/service.d
	mkdir -p -- $(XDG_DATA_HOME)/systemd/user/notify-email@.service.d
	cp -p -- systemd/user/notify-email@.service.d/50-notify-email.conf \
		$(XDG_DATA_HOME)/systemd/user/notify-email@.service.d
	mkdir -p -- $(XDG_DATA_HOME)/systemd/user/run-.service.d
	cp -p -- systemd/user/run-.service.d/50-notify-email.conf \
		$(XDG_DATA_HOME)/systemd/user/run-.service.d

install-tidy: install-sh
	cp -p -- tidy/profile.d/tidy.sh $(HOME)/.profile.d
	mkdir -p -- $(XDG_CONFIG_HOME)/tidy
	cp -p -- tidy/tidyrc $(XDG_CONFIG_HOME)/tidy/tidyrc

install-tmux: tmux/bin/tmux tmux/tmux.conf install-systemd
	cp -p -- tmux/bin/tmux $(HOME)/.local/bin
	cp -p -- tmux/profile.d/tmux.sh $(HOME)/.profile.d
	cp -p -- tmux/tmux.conf $(HOME)/.tmux.conf
	cp -p -- tmux/systemd/user/tmux.service \
		$(XDG_DATA_HOME)/systemd/user

VIM = vim
VIMDIR = $(HOME)/.vim
VIMRC = $(VIMDIR)/vimrc

install-vim:
	mkdir -p -- $(VIMDIR)
	cp -p -- vim/vimrc.stub $(HOME)/.vimrc
	cp -p -- vim/vimrc $(VIMRC)
	cp -p -- vim/filetype.vim \
		vim/scripts.vim \
		$(VIMDIR)
	cp -pR -- vim/after \
		vim/autoload \
		vim/compiler \
		vim/ftplugin \
		vim/indent \
		vim/plugin \
		vim/syntax \
		$(VIMDIR)
	for ent in vim/bundle/*/* ; do \
		[ -d "$$ent" ] || continue ; \
		cp -pR -- "$$ent" $(VIMDIR) ; \
	done

GVIMRC = $(HOME)/.gvimrc

install-vim-gui: install-vim
	cp -p -- vim/gvimrc $(GVIMRC)

install-vint:
	cp -p -- vint/vintrc.yaml $(HOME)/.vintrc.yaml

install-wget: install-sh
	cp -p -- wget/profile.d/wget.sh $(HOME)/.profile.d
	mkdir -p -- $(XDG_CACHE_HOME)/wget $(XDG_CONFIG_HOME)/wget
	cp -p -- wget/wgetrc $(XDG_CONFIG_HOME)/wget/wgetrc

install-x: check-x install-logrotate install-sh
	cp -p -- x/XCompose $(HOME)/.XCompose
	cp -p -- x/Xresources $(HOME)/.Xresources
	cp -p -- x/shrc.d/x.sh $(HOME)/.shrc.d
	mkdir -p -- $(HOME)/.xsession.d

install-xsession: x/xsession check-xsession install-x
	cp -p -- x/xsession $(HOME)/.xsession
	mkdir -p -- $(XDG_CONFIG_HOME)/log/xsession
	cp -p -- x/logrotate/config.d/xsession $(XDG_CONFIG_HOME)/logrotate/config.d

install-zsh: check-zsh install-sh
	mkdir -p -- $(HOME)/.zshrc.d
	cp -p -- zsh/profile.d/zsh.sh $(HOME)/.profile.d
	cp -p -- zsh/zprofile $(HOME)/.zprofile
	cp -p -- zsh/zshrc $(HOME)/.zshrc
	cp -p -- zsh/zshrc.d/*.zsh $(HOME)/.zshrc.d

check: check-bin \
	check-git-template-hooks \
	check-login-shell \
	check-man \
	check-sh

check-bash: bin/han
	sh check/bash.sh

check-bin: $(BINS)
	sh check/bin.sh

check-games: $(GAMES)
	sh check/games.sh

check-git-template-hooks: $(GIT_TEMPLATE_HOOKS)
	sh check/git-template-hooks.sh

check-man:
	sh check/man.sh

check-ksh:
	sh check/ksh.sh

check-login-shell:
	sh check/login-shell.sh

check-sh:
	sh check/sh.sh

check-x:
	sh check/x.sh

check-xsession: x/xsession
	sh check/xsession.sh

check-zsh:
	sh check/zsh.sh

lint: lint-bash \
	lint-bin \
	lint-games \
	lint-git-template-hooks \
	lint-ksh \
	lint-sh \
	lint-vim \
	lint-x

lint-bash: check-bash
	sh lint/bash.sh

lint-bin: check-bin
	sh lint/bin.sh

lint-games: check-games
	sh lint/games.sh

lint-git-template-hooks: check-git-template-hooks
	sh lint/git-template-hooks.sh

lint-ksh: check-ksh
	sh lint/ksh.sh

lint-sh: check-sh
	sh lint/sh.sh

lint-vim:
	sh lint/vim.sh

lint-x: check-x
	sh lint/x.sh

lint-xsession: x/xsession check-xsession
	sh lint/xsession.sh
